package uz.pdp.filter;

import uz.pdp.model.User;
import uz.pdp.utils.AppConstants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {
        "/users",
        "/users/**",
})
public class UsersFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        //authorization
        HttpSession session = request.getSession(false);
        String uri = request.getRequestURI();

        User user = (User) session.getAttribute(AppConstants.SESSION_KEY_ATTR);
        switch (user.getRole()) {
            case ROLE_USER -> {
                if (!AppConstants.USER_PAGES.contains(uri)) {
                    writeError(response);
                    return;
                }
            }
            case ROLE_MODER -> {
                if (!AppConstants.MODER_PAGES.contains(uri)) {
                    writeError(response);
                    return;
                }
            }
        }
        filterChain.doFilter(request, response);
    }

    private void writeError(HttpServletResponse response) {
        try {
//            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            response.getWriter().write("""
                    {success:"false",
                    message:"Oka huquq yo'q",
                    statusCode:"403"
                    }
                    """);
            response.setContentType("application/json");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
