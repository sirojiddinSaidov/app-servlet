package uz.pdp.filter;

import uz.pdp.utils.AppConstants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {
        "/*",
        "/**",
},
        filterName = "0")
public class MainFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) servletRequest;

        if (!AppConstants.openPages.contains(req.getRequestURI())) {
            HttpSession session = req.getSession(false);
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            if (session == null || session.getAttribute(AppConstants.SESSION_KEY_ATTR) == null) {
                response.sendRedirect("/login");
                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
