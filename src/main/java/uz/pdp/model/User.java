package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import uz.pdp.enums.RoleEnum;

@Getter
@Builder
public class User {

    private Integer id;

    private String name;

    private String username;

    private RoleEnum role;

    private String password;

    private String prePassword;
}
