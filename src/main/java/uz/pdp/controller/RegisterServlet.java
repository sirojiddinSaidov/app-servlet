package uz.pdp.controller;

import uz.pdp.service.DBService;
import uz.pdp.model.ApiResult;
import uz.pdp.model.User;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegisterServlet extends HttpServlet {


    DBService dbService;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext servletContext = getServletContext();
        System.out.println(servletContext.getAttribute("name"));
//        resp.sendRedirect("register.jsp");
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String prePassword = req.getParameter("prePassword");

        if (!password.equals(prePassword)) {
            req.setAttribute("errorMessage", "Password not match");
            req.getRequestDispatcher("register.jsp").include(req, resp);
            return;
        }
        User user = User.builder()
                .password(password)
                .name(name)
                .prePassword(prePassword)
                .username(username)
                .build();

        initializeDbService();

        ApiResult apiResult = dbService.register(user);
        if (apiResult.isSuccess()) {
            resp.sendRedirect("cabinet?name=" + name);
        } else {
            req.setAttribute("errorMessage", apiResult.getMessage());
            req.getRequestDispatcher("register.jsp").include(req, resp);
        }
    }

    private void initializeDbService() {
        if (dbService == null) {
            ServletConfig servletConfig = getServletConfig();
            dbService = new DBService(
                    servletConfig.getInitParameter("dbUrl"),
                    servletConfig.getInitParameter("dbUsername"),
                    servletConfig.getInitParameter("dbPassword")
            );
        }
    }
}