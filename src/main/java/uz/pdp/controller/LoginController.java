package uz.pdp.controller;

import uz.pdp.model.ApiResult;
import uz.pdp.model.User;
import uz.pdp.service.DBService;
import uz.pdp.utils.AppConstants;
import uz.pdp.utils.MyBean;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class LoginController extends HttpServlet {

    private DBService dbService;

    public LoginController() {
        this.dbService = (DBService) MyBean.beans.get("uz.pdp.service.DBService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        User user = dbService.login(username, password);
        if (user == null) {
            req.setAttribute("errorMessage", "Login or password incorrect");
            req.getRequestDispatcher("login.jsp").include(req, resp);
            return;
        }

//            createAndAddCookie(resp, user);
        createSession(req, user);
        resp.sendRedirect("/cabinet");


    }

    private void createSession(HttpServletRequest request, User user) {
        HttpSession session = request.getSession();
        session.setAttribute(AppConstants.SESSION_KEY_ATTR, user);
    }

    private void createAndAddCookie(HttpServletResponse resp, User user) {
        ServletContext servletContext = getServletContext();
        Cookie cookie = new Cookie(
                servletContext.getInitParameter("cookieName"),
                user.getId().toString()
        );
        cookie.setMaxAge(864000);
        resp.addCookie(cookie);
    }
}
