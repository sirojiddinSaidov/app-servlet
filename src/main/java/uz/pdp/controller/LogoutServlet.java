package uz.pdp.controller;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        request.getSession().invalidate();
        resp.sendRedirect("/login");

//        ServletContext servletContext = getServletContext();
//        String cookieName = servletContext.getInitParameter("cookieName");
//        Cookie[] cookies = request.getCookies();
//        if (cookies != null)
//            for (Cookie cookie : request.getCookies()) {
//                if (cookie.getName().equals(cookieName)) {
//                    System.out.println(cookie.getValue());
//                    cookie.setMaxAge(0);
//                    resp.addCookie(cookie);
//                    resp.sendRedirect("login.jsp");
//                    return;
//                }
//            }
    }
}
