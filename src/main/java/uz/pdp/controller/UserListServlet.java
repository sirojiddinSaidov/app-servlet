package uz.pdp.controller;

import uz.pdp.model.User;
import uz.pdp.service.DBService;
import uz.pdp.utils.MyBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

@WebServlet(urlPatterns = "/users")
public class UserListServlet extends HttpServlet {
    private final DBService dbService;

    public UserListServlet() {
        this.dbService = (DBService) MyBean.beans.get("uz.pdp.service.DBService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int size = Integer.parseInt(Objects.requireNonNullElse(req.getParameter("size"), "10"));
        int page = Integer.parseInt(Objects.requireNonNullElse(req.getParameter("page"), "0"));
        //users
        List<User> users = dbService.getUsers(page, size);
        req.setAttribute("userList", users);
        req.getRequestDispatcher("users.jsp").include(req, resp);
    }
}
