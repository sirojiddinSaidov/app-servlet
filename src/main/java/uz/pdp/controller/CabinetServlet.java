package uz.pdp.controller;

import uz.pdp.model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(value = "/cabinet",
        initParams = {
                @WebInitParam(name = "username",
                        value = "postgres"),
                @WebInitParam(name = "username",
                        value = "postgres")
        }
)
public class CabinetServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.setAttribute("userForJSP", session.getAttribute("user"));
        request.getRequestDispatcher("cabinet.jsp")
                .include(request, response);

//        ServletContext servletContext = getServletContext();
//        String cookieName = servletContext.getInitParameter("cookieName");
//        Cookie[] cookies = request.getCookies();
//        if (cookies != null)
//            for (Cookie cookie : request.getCookies()) {
//                if (cookie.getName().equals(cookieName)) {
//                    System.out.println(cookie.getValue());
//                    request.setAttribute("id", cookie.getValue());
//                    request.getRequestDispatcher("cabinet.jsp")
//                            .include(request, response);
//                    return;
//                }
//            }
//        response.sendRedirect("login.jsp");
    }

//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        HttpSession session = request.getSession();
//        Object sessionAttribute = session.getAttribute("id");
//        if (sessionAttribute == null)
//            response.sendRedirect("login.jsp");
//        else {
//            User user = (User) sessionAttribute;
//            request.setAttribute("user", user);
//            request.getRequestDispatcher("cabinet.jsp")
//                    .include(request, response);
//        }
//    }
}
