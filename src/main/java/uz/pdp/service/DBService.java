package uz.pdp.service;

import lombok.RequiredArgsConstructor;
import uz.pdp.enums.RoleEnum;
import uz.pdp.model.ApiResult;
import uz.pdp.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class DBService {

    private final String url;
    private final String user;
    private final String password;

    public Connection connect() {
        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return conn;
    }

    public List<String> getUsername() {
        Connection connect = connect();
        try (Statement statement = connect.createStatement()) {
            String sql = "SELECT username FROM users WHERE id = " + 10 + ";";
            ResultSet resultSet = statement.executeQuery(sql);
            List<String> res = new ArrayList<>();
            while (resultSet.next())
                res.add(resultSet.getString("username"));
            return res;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ApiResult register(User user) {
        try (Connection connection = connect()) {
//            PreparedStatement checkUsername = connection.prepareStatement("SELECT username FROM users WHERE username=?");
//            checkUsername.setString(1, user.getUsername());
//            ResultSet resultSet = checkUsername.executeQuery();
//            if (resultSet.next())
//                return ApiResult.builder()
//                        .success(false)
//                        .message("Already exists")
//                        .build();
//
//            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users(name,password, username)VALUES (?,?,?)");
//            preparedStatement.setString(1, user.getName());
//            preparedStatement.setString(2, user.getPassword());
//            preparedStatement.setString(3, user.getUsername());
//            boolean execute = preparedStatement.execute();
//            System.out.println(execute);


//            PreparedStatement callableStatement = connection.prepareStatement("SELECT * FROM sign_up(?,?,?)");
//            callableStatement.setString(1, user.getName());
//            callableStatement.setString(2, user.getUsername());
//            callableStatement.setString(3, user.getPassword());
//            ResultSet resultSet = callableStatement.executeQuery();
//            resultSet.next();

            CallableStatement callableStatement = connection.prepareCall("{CALL sign_up(?,?,?)}");
            callableStatement.setString(1, user.getName());
            callableStatement.setString(2, user.getUsername());
            callableStatement.setString(3, user.getPassword());
            callableStatement.registerOutParameter(1, Types.BOOLEAN);
            callableStatement.registerOutParameter(2, Types.VARCHAR);
            callableStatement.execute();

            return ApiResult.builder()
                    .success(callableStatement.getBoolean(1))
                    .message(callableStatement.getString(2))
                    .build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public User login(String username, String password) {
        try (Connection connection = connect()) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE username = ? AND password = ?");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next())
                return null;
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            username = resultSet.getString("username");
            RoleEnum role = RoleEnum.valueOf(resultSet.getString("role"));
            return User.builder()
                    .name(name)
                    .id(id)
                    .role(role)
                    .username(username)
                    .build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<User> getUsers(int page, int size) {
        return List.of(User.builder()
                        .name("ketmon")
                        .username("tesha").build(),
                User.builder()
                        .name("ketmon")
                        .username("tesha").build()
        );
    }

    //register
    //signin
}
