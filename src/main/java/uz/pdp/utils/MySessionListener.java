//package uz.pdp.utils;
//
//import javax.servlet.annotation.WebListener;
//import javax.servlet.http.HttpSession;
//import javax.servlet.http.HttpSessionEvent;
//import javax.servlet.http.HttpSessionListener;
//
//@WebListener
//public class MySessionListener implements HttpSessionListener {
//    @Override
//    public void sessionCreated(HttpSessionEvent se) {
//        HttpSession session = se.getSession();
//        session.setMaxInactiveInterval(5);
//        System.out.println("Create: " + session.getId());
//    }
//
//    @Override
//    public void sessionDestroyed(HttpSessionEvent se) {
//        HttpSession session = se.getSession();
//        System.out.println("Destroy: " + session.getId());
//    }
//}
