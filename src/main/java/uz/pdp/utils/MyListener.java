package uz.pdp.utils;

import uz.pdp.service.DBService;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

@WebListener
public class MyListener implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        createBeans(sce);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("O'ldik");
    }

    private void createBeans(ServletContextEvent servletContextEvent) {

        Object o = null;
        try {
            ServletContext servletContext = servletContextEvent.getServletContext();

            Class<?> aClass = Class.forName("uz.pdp.service.DBService");
            Constructor<?> declaredConstructor = aClass.getDeclaredConstructor(String.class, String.class, String.class);
            DBService dbService = (DBService) declaredConstructor.newInstance(
                    servletContext.getInitParameter("dbUrl"),
                    servletContext.getInitParameter("dbUsername"),
                    servletContext.getInitParameter("dbPassword"));
            MyBean.beans.put("uz.pdp.service.DBService", dbService);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        System.out.println(o);
    }

}
