package uz.pdp.utils;

import uz.pdp.model.User;

import java.util.HashSet;
import java.util.Set;

public interface AppConstants {

    Set<String> openPages = new HashSet<>() {{
        add("/login");
        add("/register");
    }};
    Set<String> USER_PAGES = new HashSet<>() {{
        add("/cabinet");
    }};
    Set<String> MODER_PAGES = new HashSet<>() {{
        addAll(USER_PAGES);
        add("/users");
    }};

    String SESSION_KEY_ATTR = "user";
}
