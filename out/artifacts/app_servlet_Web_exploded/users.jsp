<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: siroj
  Date: 8/24/2023
  Time: 9:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<h1>UsersList</h1>
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Username</th>
        <th scope="col" colspan="2">Action</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach
            items="${requestScope.get('userList')}"
            var="user" varStatus="loop">

        <tr>
            <th scope="row">${loop.index + 1}</th>
            <td>${user.name}</td>
            <td>${user.username}</td>
            <td>Edit</td>
            <td>Delete</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>
